FROM maven:3.5.3-jdk-8-slim AS build

COPY . /tmp/app

WORKDIR /tmp/app

RUN mvn clean package

FROM openjdk:8-jdk-alpine

COPY --from=build /tmp/app/target/app.jar /tmp/app.jar

ENTRYPOINT ["java", "-jar", "/tmp/app.jar"]
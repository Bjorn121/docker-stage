# Docker Multi-Stage Build

This is a sample to create a multi-stage build with Docker, Java and Spring Boot.

## Commands

To create the image, simply run `docker image build -t stage .`.  Add `--no-cache` if you want to override the Docker cache.

To run the container, run `docker container run -it -d -p 80:8080 stage:latest`.  This exposes the container on port 80 
on your local machine.  If you want to use a different port, change 80 to the value you desire.